package entity

import "ferromagnetic/component"

type Tile struct {
	component.Pos
	component.Sprite
}
