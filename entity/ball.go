package entity

import "ferromagnetic/component"

type Ball struct {
	component.Pos
	component.Vel
	component.Radius
	component.Slide
	component.Gravity
	component.Death
}
