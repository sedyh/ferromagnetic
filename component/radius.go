package component

type Radius struct {
	Value float64
}

func NewRadius(rad float64) Radius {
	return Radius{rad}
}
