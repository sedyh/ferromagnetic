package component

import "ferromagnetic/helper/enum"

type Slide struct {
	Target   Pos
	Progress float64
	State    enum.BallState
}

func NewSliding() Slide {
	return Slide{Progress: 0, State: enum.BallStateSeek}
}
