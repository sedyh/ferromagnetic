package assets

import (
	"embed"
	"image"
	"image/color"
	"time"

	"github.com/hajimehoshi/ebiten/v2/audio"

	"ferromagnetic/helper/graphics"
	"ferromagnetic/helper/load"
	"ferromagnetic/helper/split"
)

const (
	Tilesize = 64
	TileD    = 96
	BaseW    = Tilesize
	BaseH    = Tilesize / 2
)

var (
	Background = color.RGBA{R: 41, G: 44, B: 45, A: 255}
	Images     = make(map[string]*graphics.Frameset)
	Masks      = make(map[string]image.Image)
	Audio      = make(map[string]*audio.Player)
	Counter    int
)

//go:embed data
var fs embed.FS

func Init() {
	tiles := load.Image(fs, "data/image/hills.png")
	Images["grass"] = split.Multi(tiles, 0, 0, Tilesize, Tilesize*2, 60, false, time.Millisecond*50)
	hole := load.Image(fs, "data/image/hole.png")
	Images["hole"] = split.Single(hole, 0, 0, 160, 128, false)
	Audio["bg"] = load.Audio(fs, "data/audio/bg.mp3")
}
