package graphics

import (
	"image/color"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
)

var circle *ebiten.Shader

func init() {
	var err error

	circle, err = ebiten.NewShader([]byte(`
        package main

        func Fragment(pos vec4, tex vec2, col vec4) vec4 {
            return col
        }
    `))
	if err != nil {
		panic(err)
	}
}

func DrawCircle(screen *ebiten.Image, x, y, radius float64, clr color.RGBA) {
	var path vector.Path

	path.MoveTo(float32(x), float32(y))
	path.Arc(float32(x), float32(y), float32(radius), 0, math.Pi*2, vector.Clockwise)

	vertices, indices := path.AppendVerticesAndIndicesForFilling(nil, nil)

	redScaled := float32(clr.R) / 255
	greenScaled := float32(clr.G) / 255
	blueScaled := float32(clr.B) / 255
	alphaScaled := float32(clr.A) / 255

	for i := range vertices {
		v := &vertices[i]

		v.ColorR = redScaled
		v.ColorG = greenScaled
		v.ColorB = blueScaled
		v.ColorA = alphaScaled
	}

	screen.DrawTrianglesShader(vertices, indices, circle, &ebiten.DrawTrianglesShaderOptions{
		FillRule: ebiten.EvenOdd,
	})
}
