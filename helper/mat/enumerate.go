package mat

import "math/rand"

func ShuffleI(values []int) int {
	if len(values) == 0 {
		return 0
	}
	res := append([]int{}, values...)
	rand.Shuffle(len(res), func(i, j int) {
		values[i], values[j] = values[j], values[i]
	})
	return res[0]
}

func RangeI(min, max int) int {
	switch {
	case min < max:
		return rand.Intn(max-min) + min
	case min > max:
		return rand.Intn(min-max) + max
	default:
		return min
	}
}

func RangeF(min, max float64) float64 {
	switch {
	case min < max:
		return min + rand.Float64()*(max-min)
	case min > max:
		return max + rand.Float64()*(min-max)
	default:
		return min
	}
}

func Chance(percent float64) bool {
	return rand.Float64() > percent
}
