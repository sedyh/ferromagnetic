package geo

import (
	"image"

	"github.com/hajimehoshi/ebiten/v2"
)

func Rect(x, y, w, h int) image.Rectangle {
	return image.Rect(x, y, x+w, y+h)
}

func Image(image *ebiten.Image, mul float64) (x, y float64) {
	return float64(image.Bounds().Dx()) * mul, float64(image.Bounds().Dy()) * mul
}
