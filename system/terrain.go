package system

import (
	"math"
	"sync"

	"ferromagnetic/component"

	"github.com/ojrac/opensimplex-go"
	"github.com/sedyh/mizu/pkg/engine"
)

type Terrain struct {
	xt, yt float64
}

func NewTerrain() *Terrain {
	return &Terrain{}
}

//var seed = time.Now().UTC().UnixNano()
//var seed int64 = 1655845617548397575
var seed int64 = 1655846815234470098
var noise = opensimplex.NewNormalized(seed)

const freq = 0.08

var ad = 0.0
var sx, sy = 0.0, 0.0
var rd = 0.0

//var ticker = time.NewTicker(5 * time.Second)

var once sync.Once

func (s *Terrain) Update(w engine.World) {
	w.View(component.Pos{}, component.Sprite{}).Each(func(e engine.Entity) {
		var pos *component.Pos
		e.Get(&pos)

		if rd < math.Pi*2 {
			rd += 0.00001
		} else {
			rd = 0
		}
		sx = math.Cos(rd) * (2.5 + ad)
		sy = math.Sin(rd) * (2.5 + ad)
		pos.Z = noise.Eval2((pos.X+sx)*freq, (pos.Y+sy)*freq)
	})
	once.Do(func() {
	})
}
