package system

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/sedyh/mizu/pkg/engine"

	"ferromagnetic/component"
)

type Focus struct {
	balls []engine.Entity
}

func NewFocus() *Focus {
	return &Focus{}
}

func (f *Focus) Update(w engine.World) {
	f.balls = f.balls[:0]
	camera, ok := w.View(component.Pos{}, component.Zoom{}).Get()
	if !ok {
		return
	}
	var pos *component.Pos
	var zoom *component.Zoom
	camera.Get(&pos, &zoom)

	avg := component.NewPos(0, 0)
	balls := w.View(component.Pos{}, component.Slide{}).Filter()

	for _, ball := range balls {
		var death *component.Death
		ball.Get(&death)

		if death.Is == true {
			continue
		}

		f.balls = append(f.balls, ball)
	}
	for _, ball := range f.balls {
		var p *component.Pos
		ball.Get(&p)

		avg.X += p.X
		avg.Y += p.Y
		avg.Z += p.Z
	}
	avg.X /= float64(len(f.balls))
	avg.Y /= float64(len(f.balls))
	avg.Z /= float64(len(f.balls))

	pos.X, pos.Y = avg.X, avg.Y

	switch {
	case zoom.Value < 2 && IsZoomIn():
		zoom.Value += 0.05
	case zoom.Value > 0.25 && IsZoomOut():
		zoom.Value -= 0.05
	case zoom.Value != 1 && IsZoomReset():
		zoom.Value = 1
	}
}

func IsZoomIn() bool {
	if ebiten.IsKeyPressed(ebiten.KeyEqual) {
		return true
	}

	if _, y := ebiten.Wheel(); y > 0 {
		return true
	}

	return false
}

func IsZoomOut() bool {
	if ebiten.IsKeyPressed(ebiten.KeyMinus) {
		return true
	}

	if _, y := ebiten.Wheel(); y < 0 {
		return true
	}

	return false
}

func IsZoomReset() bool {
	if ebiten.IsKeyPressed(ebiten.KeyGraveAccent) {
		return true
	}

	if ebiten.IsMouseButtonPressed(ebiten.MouseButtonMiddle) {
		return true
	}

	return false
}
