package system

import (
	"github.com/sedyh/mizu/pkg/engine"

	"ferromagnetic/component"
)

type Animation struct {
	*component.Sprite
}

func NewAnimation() *Animation {
	return &Animation{}
}

func (a *Animation) Update(_ engine.World) {
	if len(a.Frameset.Images) < 2 {
		return
	}

	select {
	case <-a.Frameset.Ticker.C:
		a.Frameset.Current = (a.Frameset.Current + 1) % len(a.Frameset.Images)
	default:
	}
}
