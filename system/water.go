package system

import (
	"math"

	"ferromagnetic/component"

	"github.com/hajimehoshi/ebiten/v2"

	"github.com/sedyh/mizu/pkg/engine"
)

type Water struct {
	waveOffset   []float32
	waveShift    float32
	waveEmersion float32
	scale        float64
	offset       float64
	fastPeriod   float64
	slowPeriod   float64
}

func NewWater() *Water {
	return &Water{
		waveOffset: make([]float32, 2),
		scale:      1.0,
	}
}

func (m *Water) Update(w engine.World) {
	if ebiten.IsKeyPressed(ebiten.KeyZ) {
		m.waveShift -= 0.1
	}
	if ebiten.IsKeyPressed(ebiten.KeyX) {
		m.waveShift += 0.1
	}
	if ebiten.IsKeyPressed(ebiten.KeyC) {
		m.scale -= 0.01
	}
	if ebiten.IsKeyPressed(ebiten.KeyV) {
		m.scale += 0.01
	}

	if m.fastPeriod < math.Pi*2 {
		m.fastPeriod += 0.1
	} else {
		m.fastPeriod = 0
	}

	if m.slowPeriod < math.Pi*2 {
		m.slowPeriod += 0.001
	} else {
		m.slowPeriod = 0
	}

	m.waveShift = float32(math.Sin(m.fastPeriod)*0.1) + 0.8
	m.waveOffset[0] = float32(math.Cos(m.slowPeriod))
	m.waveOffset[1] = float32(math.Sin(m.slowPeriod))
	m.waveEmersion = float32(math.Sin(m.slowPeriod))
}

func (m *Water) Draw(w engine.World, screen *ebiten.Image) {
	camera, ok := w.View(component.Pos{}, component.Zoom{}).Get()
	if !ok {
		return
	}
	var cameraPos *component.Pos
	var cameraZoom *component.Zoom
	camera.Get(&cameraPos, &cameraZoom)

	sw, sh := float64(screen.Bounds().Dx()), float64(screen.Bounds().Dy())

	op := &ebiten.DrawRectShaderOptions{}
	op.Uniforms = make(map[string]any)
	op.GeoM.Translate(-sw*m.scale*0.5, -sh*m.scale*0.5)
	op.GeoM.Scale(1, 0.5)
	op.Uniforms["ScreenSize"] = []float32{
		float32(int(sw * m.scale * cameraZoom.Value * 2)),
		float32(int(sh * m.scale * cameraZoom.Value * 2)),
	}
	op.Uniforms["WaveShift"] = m.waveShift
	op.Uniforms["WaveOffset"] = m.waveOffset
	op.Uniforms["WaveEmersion"] = m.waveEmersion
	op.Uniforms["Scale"] = float32(2 - cameraZoom.Value*0.5)
	screen.DrawRectShader(int(sw*m.scale*2), int(sh*m.scale*4), watershader, op)
}

var watershader *ebiten.Shader

func init() {
	var err error
	watershader, err = ebiten.NewShader([]byte(`
		package main

		var ScreenSize vec2
		var WaveShift float
		var WaveOffset vec2
		var WaveEmersion float
		var Scale float

		func permute(x vec3) vec3 {
			return mod((34.0 * x + 1.0) * x, 289.0)
		}
		
		func dist(x vec3, y vec3, manhattanDistance bool) vec3 {
			if manhattanDistance {
				return abs(x) + abs(y)
			} else {
				return (x * x + y * y)
			}
		}
		
		func worley(P vec2, jitter float, manhattanDistance bool) vec2 {
			K := 0.142857142857 
			Ko := 0.428571428571 
			Pi := mod(floor(P), 289.0)
			Pf := fract(P)
			oi := vec3(-1.0, 0.0, 1.0)
			of := vec3(-0.5, 0.5, 1.5)
			px := permute(Pi.x + oi)
			p := permute(px.x + Pi.y + oi) 
			ox := fract(p*K) - Ko
			oy := mod(floor(p*K),7.0)*K - Ko
			dx := Pf.x + 0.5 + jitter*ox
			dy := Pf.y - of + jitter*oy
			d1 := dist(dx,dy, manhattanDistance)
			p = permute(px.y + Pi.y + oi) 
			ox = fract(p*K) - Ko
			oy = mod(floor(p*K),7.0)*K - Ko
			dx = Pf.x - 0.5 + jitter*ox
			dy = Pf.y - of + jitter*oy
			d2 := dist(dx,dy, manhattanDistance) 
			p = permute(px.z + Pi.y + oi) 
			ox = fract(p*K) - Ko
			oy = mod(floor(p*K),7.0)*K - Ko
			dx = Pf.x - 1.5 + jitter*ox
			dy = Pf.y - of + jitter*oy
			d3 := dist(dx,dy, manhattanDistance) 
			d1a := min(d1, d2)
			d2 = max(d1, d2) 
			d2 = min(d2, d3) 
			d1 = min(d1a, d2) 
			d2 = max(d1a, d2)
			if d1.x >= d1.y {
				d1.xy = d1.yx
			}
			if d1.x >= d1.z {
				d1.xz = d1.zx
			}
			d1.yz = min(d1.yz, d2.yz) 
			d1.y = min(d1.y, d1.z) 
			d1.y = min(d1.y, d2.x) 
			return sqrt(d1.xy)
		}
		
		func random(c vec3) vec3 {
			j := 4096.0*sin(dot(c, vec3(17.0, 59.4, 15.0)))
			
			var r vec3
			r.z = fract(512.0*j)
			j *= .125
			r.x = fract(512.0*j)
			j *= .125
			r.y = fract(512.0*j)
		
			return r-0.5;
		}

		func simplex(p vec3) float {
			 F3 := 0.3333333
			 G3 := 0.1666667
			 s := floor(p + dot(p, vec3(F3)))
			 x := p - s + dot(s, vec3(G3))
			 
			 e := step(vec3(0.0), x - x.yzx)
			 i1 := e*(1.0 - e.zxy)
			 i2 := 1.0 - e.zxy*(1.0 - e)
				
			 x1 := x - i1 + G3
			 x2 := x - i2 + 2.0*G3
			 x3 := x - 1.0 + 3.0*G3
			 
			 var w, d vec4
			 
			 w.x = dot(x, x)
			 w.y = dot(x1, x1)
			 w.z = dot(x2, x2)
			 w.w = dot(x3, x3)
			 
			 w = max(0.6 - w, 0.0)
			 
			 d.x = dot(random(s), x)
			 d.y = dot(random(s + i1), x1)
			 d.z = dot(random(s + i2), x2)
			 d.w = dot(random(s + 1.0), x3)
			 
			 w *= w
			 w *= w
			 d *= w
			 
			 return dot(d, vec4(52.0))
		}

		func Fragment(pos vec4, tex vec2, col vec4) vec4 {
			p := tex / ScreenSize
			p.y *= ScreenSize.y / ScreenSize.x

			sc := simplex(vec3(p.xy*15, WaveEmersion))
			f := worley((p.xy+WaveOffset.xy+vec2(sc*0.01))*10, WaveShift, false)
			wc := f.y-f.x
			ff := worley((p.xy+WaveOffset.xy+vec2(sc*0.01))*15+5, WaveShift, false)
			wc2 := ff.y-ff.x
			
			wave := vec4(.07, .31, .5, 1)
			foam := vec4(1, 1, 1, 1)
			layer := 0.6
			start, end := 0.08*Scale, 0.05*Scale

			d := mix(wave, foam, smoothstep(start, end, wc))
			d = mix(d, wave, layer)
			d = mix(d, foam, smoothstep(start, end, wc2))
			d = mix(d, wave, layer)
			
			return d
		}
	`))
	if err != nil {
		panic(err)
	}
}
