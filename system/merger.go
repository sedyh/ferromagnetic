package system

import (
	"ferromagnetic/component"
	"ferromagnetic/helper/mat"
	"ferromagnetic/helper/vector"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"

	"github.com/sedyh/mizu/pkg/engine"
)

type Merger struct {
	merge bool
	balls []engine.Entity
}

func NewMerger() *Merger {
	return &Merger{}
}

func (m *Merger) Update(w engine.World) {
	m.balls = m.balls[:0]

	if inpututil.IsKeyJustPressed(ebiten.KeyZ) {
		m.merge = !m.merge
	}

	balls := w.View(component.Pos{}, component.Slide{}).Filter()
	for _, ball := range balls {
		var death *component.Death
		ball.Get(&death)

		if death.Is == true {
			continue
		}

		m.balls = append(m.balls, ball)
	}
	if inpututil.IsKeyJustPressed(ebiten.KeyS) {
		camera, ok := w.View(component.Pos{}, component.Zoom{}).Get()
		if !ok {
			return
		}
		var pos *component.Pos
		camera.Get(&pos)

		for _, ball := range m.balls {
			var p *component.Pos
			var v *component.Vel

			ball.Get(&p, &v)

			dirX := pos.X - p.X
			dirY := pos.Y - p.Y
			dirZ := pos.Z - p.Z

			l, m1, n := vector.Normalize(dirX, dirY, dirZ)
			v.L -= l * 0.35
			v.M -= m1 * 0.35
			v.N -= n * 0.35

		}
	}

	if m.merge {
		camera, ok := w.View(component.Pos{}, component.Zoom{}).Get()
		if !ok {
			return
		}
		var pos *component.Pos
		camera.Get(&pos)

		for _, ball := range m.balls {
			var p *component.Pos
			var v *component.Vel
			ball.Get(&p, &v)

			dirX := pos.X - p.X
			dirY := pos.Y - p.Y
			dirZ := pos.Z - p.Z

			dist := vector.Length(dirX, dirY, dirZ)

			l, m1, k := vector.NormalizeLength(dirX, dirY, dirZ, dist)

			v.L += l * mat.Lerp(0.01, dist, 0.01)
			v.M += m1 * mat.Lerp(0.01, dist, 0.01)
			v.N += k * mat.Lerp(0.01, dist, 0.01)
		}
	}
}
