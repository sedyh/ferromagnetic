package system

import (
	"github.com/sedyh/mizu/pkg/engine"

	"ferromagnetic/component"
)

type Gravitation struct {
	*component.Gravity
	*component.Vel
	*component.Slide
	*component.Pos
}

func NewGravitation() *Gravitation {
	return &Gravitation{}
}

func (g *Gravitation) Update(_ engine.World) {
	g.Vel.N -= g.Gravity.Value
}
