package system

import (
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/sedyh/mizu/pkg/engine"

	"ferromagnetic/assets"
	"ferromagnetic/component"
	"ferromagnetic/helper/geo"
	"ferromagnetic/helper/isometry"
	"ferromagnetic/helper/mat"
)

type Selection struct{}

func NewSelection() *Selection {
	return &Selection{}
}

var prevSelected = component.NewPos(0, 0, 0)
var selected = component.NewPos(0, 0, 0)

func (s *Selection) Update(w engine.World) {
	w.View(component.Pos{}, component.Sprite{}).Each(func(e engine.Entity) {
		var pos *component.Pos
		e.Get(&pos)

		pos.OffsetB = 0
		if mat.InCircleIso(selected, 4, *pos) {
			if ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
				selScreen := isometry.ToScreen(selected)
				prevScreen := isometry.ToScreen(prevSelected)
				angle := mat.PointsAngle(selScreen.X, selScreen.Y, prevScreen.X, prevScreen.Y)
				angleGrad := angle * 180 / math.Pi

				//left
				if angleGrad > -25.5 && angleGrad < 25.5 {
					x := (pos.X - pos.Y) - (selected.X - selected.Y) + 10
					s.grow(pos, mat.Normalize(x*x/4, 0, 80, 0, 1))
				}
				//right
				if (angleGrad > 157.5 && angleGrad < 180) || (angleGrad > -180 && angleGrad < -157.5) {
					x := (pos.X - pos.Y) - (selected.X - selected.Y) - 10
					s.grow(pos, mat.Normalize(x*x/4, 0, 80, 0, 1))
				}
				//top
				if angleGrad > 67.5 && angleGrad < 112.5 {
					x := (pos.X + pos.Y) - (selected.X + selected.Y) + 10
					s.grow(pos, mat.Normalize(x*x/4, 0, 80, 0, 1))
				}
				//down
				if angleGrad < -67.5 && angleGrad > -112.5 {
					x := (pos.X + pos.Y) - (selected.X + selected.Y) - 10
					s.grow(pos, mat.Normalize(x*x/4, 0, 80, 0, 1))
				}
				//top left
				if angleGrad > 22.5 && angleGrad < 67.5 {
					x := pos.X - selected.X + 10
					s.grow(pos, mat.Normalize(x*x/4, 0, 80, 0, 1))
				}
				//down left
				if angleGrad < -22.5 && angleGrad > -67.5 {
					x := pos.Y - selected.Y - 10
					s.grow(pos, mat.Normalize(x*x/4, 0, 80, 0, 1))
				}
				//top right
				if angleGrad > 112.5 && angleGrad < 157.5 {
					x := pos.Y - selected.Y + 10
					s.grow(pos, mat.Normalize(x*x/4, 0, 80, 0, 1))
				}
				//down right
				if angleGrad < -112.5 && angleGrad > -157.5 {
					x := selected.X - pos.X + 10
					s.grow(pos, mat.Normalize(x*x/4, 0, 80, 0, 1))
				}
			}
			pos.OffsetB = pos.Grow
			return
		}
		pos.OffsetB = mat.Lerp(pos.OffsetB, 0, 0.02)
		if mat.Equal(pos.OffsetB, 0, 0.001) {
			pos.OffsetB = 0
		}
		pos.Grow = mat.Lerp(pos.Grow, 0, 0.02)
		pos.GrowAcc = mat.Lerp(pos.GrowAcc, 0, 0.02)
	})
	prevSelected = selected
}

func (s *Selection) Draw(w engine.World, screen *ebiten.Image) {
	camera, ok := w.View(component.Pos{}, component.Zoom{}).Get()
	if !ok {
		return
	}
	var cameraPos *component.Pos
	var cameraZoom *component.Zoom
	camera.Get(&cameraPos, &cameraZoom)

	cursor := component.NewPos(ebiten.CursorPosition())
	scale := component.NewPos(cameraZoom.Value, cameraZoom.Value)
	scaledTilesize := component.NewPos(assets.Tilesize, assets.Tilesize).Mul(scale)
	scaledIsoOffset := isometry.ToScreen(cameraPos.Mul(scaledTilesize))
	halvedScreenBounds := component.NewPos(geo.Image(screen, 0.5))
	scaledBase := component.NewPos(assets.BaseW, assets.BaseH).Mul(scale)
	before := cursor.Add(scaledIsoOffset).Sub(halvedScreenBounds).Div(scaledBase)
	before.Y += 1
	selected = component.NewPos(int(before.Y+before.X), int(before.Y-before.X), 0)
}

func (s *Selection) grow(pos *component.Pos, y float64) {
	if pos.Grow < y {
		pos.GrowAcc += 0.01
		pos.Grow += pos.GrowAcc
	}

}
