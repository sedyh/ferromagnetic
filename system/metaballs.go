package system

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/sedyh/mizu/pkg/engine"

	"ferromagnetic/assets"
	"ferromagnetic/component"
	"ferromagnetic/helper/isometry"
)

type Metaballs struct {
	cutThreshold  float32
	ballMagnitude float32
}

func NewMetaballs() *Metaballs {
	return &Metaballs{
		cutThreshold:  0.23,
		ballMagnitude: 28.0,
	}
}

func (m *Metaballs) Update(w engine.World) {
	if ebiten.IsKeyPressed(ebiten.KeyQ) {
		m.cutThreshold -= 0.01
	}
	if ebiten.IsKeyPressed(ebiten.KeyE) {
		m.cutThreshold += 0.01
	}
	if ebiten.IsKeyPressed(ebiten.KeyR) {
		m.ballMagnitude -= 0.1
	}
	if ebiten.IsKeyPressed(ebiten.KeyT) {
		m.ballMagnitude += 0.1
	}
}

func (m *Metaballs) Draw(w engine.World, screen *ebiten.Image) {
	camera, ok := w.View(component.Pos{}, component.Zoom{}).Get()
	if !ok {
		return
	}
	var cameraPos *component.Pos
	var cameraZoom *component.Zoom
	camera.Get(&cameraPos, &cameraZoom)

	offset := isometry.ToScreen(component.NewPos(cameraPos.X*assets.Tilesize, cameraPos.Y*assets.Tilesize))

	var xs, ys []float32
	w.View(component.Pos{}, component.Vel{}, component.Slide{}).Each(func(e engine.Entity) {
		var pos *component.Pos
		var vel *component.Vel
		var sliding *component.Slide
		e.Get(&pos, &vel, &sliding)

		iso := isometry.ToScreen(component.NewPos(pos.X*assets.Tilesize, pos.Y*assets.Tilesize))
		iso.Y -= (pos.Z + pos.OffsetA + pos.OffsetB) * assets.TileD
		iso.X -= offset.X
		iso.Y -= offset.Y
		iso.X *= cameraZoom.Value
		iso.Y *= cameraZoom.Value
		iso.X += float64(screen.Bounds().Dx()) / 2
		iso.Y += float64(screen.Bounds().Dy()) / 2
		iso.Y += assets.TileD * 0.65 * cameraZoom.Value

		xs = append(xs, float32(iso.X))
		ys = append(ys, float32(iso.Y))
	})

	op := &ebiten.DrawRectShaderOptions{}
	op.Uniforms = make(map[string]any)
	op.Uniforms["XS"] = xs
	op.Uniforms["YS"] = ys
	op.Uniforms["Threshold"] = m.cutThreshold * 1 / float32(cameraZoom.Value)

	op.Uniforms["Magnitude"] = m.ballMagnitude * 1.5 * float32(cameraZoom.Value)
	op.Uniforms["ColorR"] = float32(155. / 255)
	op.Uniforms["ColorG"] = float32(171. / 255)
	op.Uniforms["ColorB"] = float32(178. / 255)
	op.Uniforms["ColorA"] = float32(0.5)
	screen.DrawRectShader(screen.Bounds().Dx(), screen.Bounds().Dy(), metashader, op)

	op.Uniforms["Magnitude"] = m.ballMagnitude * float32(cameraZoom.Value)
	op.Uniforms["ColorR"] = float32(199. / 255)
	op.Uniforms["ColorG"] = float32(220. / 255)
	op.Uniforms["ColorB"] = float32(208. / 255)
	op.Uniforms["ColorA"] = float32(1.0)
	screen.DrawRectShader(screen.Bounds().Dx(), screen.Bounds().Dy(), metashader, op)
}

var metashader *ebiten.Shader

func init() {
	var err error

	metashader, err = ebiten.NewShader([]byte(`
		package main

		const size = 20
		var XS [size]float
		var YS [size]float
		var Magnitude float
		var Threshold float
		var ColorR float
		var ColorG float
		var ColorB float
		var ColorA float

		func dist(x0, y0, x1, y1, m float) float {
			return m / pow(sqrt(pow(x0-x1, 2) + pow(y0-y1, 2)), 2)
		}

		func total(x, y, m float) float {
			sum := 0.0
			for i:=0; i<size; i++ {
				sum += dist(x, y, XS[i], YS[i], m)
			}
			return sum
		}

		func cut(sum, cutThreshold, t, f float) float {
			if sum < cutThreshold {
				return t
			}
			return f
		}

		func Fragment(pos vec4, tex vec2, col vec4) vec4 {
			return vec4(
				cut(total(pos.x, pos.y, Magnitude), Threshold, 0, ColorR),
				cut(total(pos.x, pos.y, Magnitude), Threshold, 0, ColorG),
				cut(total(pos.x, pos.y, Magnitude), Threshold, 0, ColorB),
				cut(total(pos.x, pos.y, Magnitude), Threshold, 0, 1),
			) * ColorA
		}
	`))
	if err != nil {
		panic(err)
	}
}
