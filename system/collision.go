package system

import (
	"ferromagnetic/helper/mat"
	"github.com/sedyh/mizu/pkg/engine"

	"ferromagnetic/component"
	"ferromagnetic/helper/vector"
)

type Collision struct {
	candidates []engine.Entity
}

func NewCollision() *Collision {
	return &Collision{}
}

func (c *Collision) Update(w engine.World) {
	c.candidates = c.candidates[:0]

	w.View(component.Pos{}, component.Vel{}, component.Radius{}, component.Slide{}).Each(func(e engine.Entity) {
		c.candidates = append(c.candidates, e)
	})

	for _, a := range c.candidates {
		for _, b := range c.candidates {
			if a.ID() == b.ID() {
				continue
			}

			var aPos *component.Pos
			var aVel *component.Vel
			var aRadius *component.Radius
			a.Get(&aPos, &aVel, &aRadius)

			var bPos *component.Pos
			var bVel *component.Vel
			var bRadius *component.Radius
			b.Get(&bPos, &bVel, &bRadius)

			distX, distY, distZ := bPos.X-aPos.X, bPos.Y-aPos.Y, bPos.Z-aPos.Z

			dist := vector.Length(distX, distY, distZ)

			if dist < aRadius.Value+bRadius.Value {
				overlap := mat.Lerp(0, 0.5*(dist-aRadius.Value-bRadius.Value), 0.1)
				dirX, dirY, dirZ := vector.NormalizeLength(distX, distY, distZ, dist)

				aVel.L += overlap * dirX
				aVel.M += overlap * dirY
				aVel.N += overlap * dirZ

				bVel.L -= overlap * dirX
				bVel.M -= overlap * dirY
				bVel.N += overlap * dirZ
			}
		}
	}
}
